//
//  FAUCheckbox.swift
//  WWApp
//
//  Created by Steven T. Norris on 1/5/15.
//  Copyright (c) 2015 FortyAU. All rights reserved.
//

import UIKit

struct FAUCheckboxStatus{
    static var CHECKED = 1
    static var UNCHECKED = 0
}

class FAUCheckbox: UILabel {
    
    var userText:String!
    var state:Int! = FAUCheckboxStatus.UNCHECKED
    
    //Sets up new checkbox
    init(){
        super.init(frame: CGRectZero)
        self.initialize()
    }

    //Sets up new checkbox
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    //Checkbox initialization helper
    private func initialize(){
        //Set as unchecked
        userText = text
        text = "\u{2610} " + userText
        state = FAUCheckboxStatus.UNCHECKED
        
        //Add gesture recognizer
        let tap = UITapGestureRecognizer(target: self, action: #selector(FAUCheckbox.checkUncheck(_:)))
        self.addGestureRecognizer(tap)
    }
    
    //Sets text (cannot set text directly due to checkbox unicode
    func setNewText(newText:String){
        userText = newText
        if(state == FAUCheckboxStatus.CHECKED){
            text = "\u{2611} "
        }
        else{
            text = "\u{2610} "
        }
        text = text! + userText
    }
    
    //Handles checks and unchecks of box
    func checkUncheck(gestureRecognizer:UITapGestureRecognizer){
        if(state == FAUCheckboxStatus.UNCHECKED){
            state = FAUCheckboxStatus.CHECKED
            self.text = "\u{2611} " + userText
        }
        else{
            state = FAUCheckboxStatus.UNCHECKED
            self.text = "\u{2610} " + userText
        }
    }

}
