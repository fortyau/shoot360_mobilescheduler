//
//  FAUErrorLabel.swift
//  CMSS
//
//  Created by Steven T. Norris on 11/12/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

import UIKit

class FAUErrorLabel: UILabel, UIGestureRecognizerDelegate {
    
//-------------------
//MARK:Structs/Enums
//-------------------
    
    //Messages with attributes
    struct MessageColor {
        var backgroundColor:UIColor
        var textColor:UIColor
    }
    
    //Message Levels
    enum MessageLevel{
        case FATAL
        case ERROR
        case WARNING
        case INFO
        case DEBUG
    }
    
    //Transiton Stage
    enum TransitionStage{
        case APPEARING
        case APPEARED
        case DISMISSING
        case DISMISSED
        case QUEUEEMPTY
        
        var description : String {
            switch self{
                case .APPEARING: return "APPEARING"
                case .APPEARED: return "APPEARED"
                case .DISMISSING: return "DISMISSING"
                case .DISMISSED: return "DISMISSED"
                case .QUEUEEMPTY: return "QUEUEEMPTY"
            }
        }
    }
    
//-------------------
//MARK:Variables
//-------------------
    
    var delay = 0.0
    var duration = 0.2
    var displayTime:UInt64 = 2
    var messageLevelAllowed = MessageLevel.INFO
    var messageLevels = [
        MessageLevel.FATAL:MessageColor(backgroundColor: UIColor.redColor(), textColor: UIColor.whiteColor()),
        MessageLevel.ERROR:MessageColor(backgroundColor: UIColor.redColor(), textColor: UIColor.whiteColor()),
        MessageLevel.WARNING:MessageColor(backgroundColor: UIColor.yellowColor(), textColor: UIColor.blackColor()),
        MessageLevel.INFO:MessageColor(backgroundColor: UIColor.blackColor(), textColor: UIColor.whiteColor()),
        MessageLevel.DEBUG:MessageColor(backgroundColor: UIColor.greenColor(), textColor: UIColor.blackColor())
    ]
    var messageQueue:Array<(text:String,level:MessageLevel)> = []
    var messageQueueCap = 5
    var transitionStage = TransitionStage.QUEUEEMPTY
    
//--------------------
//MARK:Utilities
//--------------------
    
    //Adds or removes a tap dismiss gesture recognizer
    func setTapDismiss(tapDismiss:Bool){
        if(tapDismiss){
            let tap = UITapGestureRecognizer(target: self, action:#selector(FAUErrorLabel.tap(_:)))
            tap.delegate = self
            self.addGestureRecognizer(tap)
        }
        else{
            if let recognizers = self.gestureRecognizers{
                for recognizer in recognizers{
                    self.removeGestureRecognizer(recognizer )
                }
            }
        }
    }
    
    //Sets colors for certain message level
    func setColorsForLevel(level:MessageLevel,backgroundColor:UIColor,textColor:UIColor){
        messageLevels[level]!.backgroundColor = backgroundColor
        messageLevels[level]!.textColor = textColor
    }
    
    //Set allowed message level
    func setMessageLevelAllowed(level:MessageLevel){
        messageLevelAllowed = level
    }
    
    //Displays a message
    func displayMessage(level:MessageLevel, message:String){
        if(messageLevelAllowed.hashValue >= level.hashValue && messageQueue.count < messageQueueCap){
            messageQueue.append(text:message, level:level)
            if(transitionStage == TransitionStage.QUEUEEMPTY){
                fadeIn()
            }
        }
    }
    
    //Gesture recognizer
    func tap(recognizer:UITapGestureRecognizer){
        dismiss()
    }
    
    //Sets message to dismiss
    func dismiss(){
        fadeOut()
    }
    
    //Fades the label in
    private func fadeIn(){
        if(messageQueue.count > 0){
            transitionStage = TransitionStage.APPEARING
            text = messageQueue[0].text
            backgroundColor = messageLevels[messageQueue[0].level]!.backgroundColor
            textColor = messageLevels[messageQueue[0].level]!.textColor
            hidden = false
            UIView.animateWithDuration(
                duration,
                delay: delay,
                options: [],
                animations: {
                    self.alpha = 1.0
                    return Void()
                }, completion: {finished in
                    self.transitionStage = TransitionStage.APPEARED
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(self.displayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), {
                        if(self.transitionStage != TransitionStage.DISMISSING && self.transitionStage != TransitionStage.DISMISSED){
                            if(self.transitionStage == TransitionStage.QUEUEEMPTY){
                                self.alpha = 0.0
                                self.hidden = true
                            }
                            else{
                                self.dismiss()
                            }
                        }
                    })
                    return Void()
                }
            )
        }
        else{
            transitionStage = TransitionStage.QUEUEEMPTY
        }
    }
    
    //Fades the label out
    private func fadeOut(){
        if(transitionStage != TransitionStage.DISMISSING && transitionStage != TransitionStage.DISMISSED){
            transitionStage = TransitionStage.DISMISSING
            UIView.animateWithDuration(
                duration,
                delay: delay,
                options: [],
                animations: {
                    self.alpha = 0.0
                    return Void()
                }, completion: {finished in
                    self.messageQueue.removeAtIndex(0)
                    self.transitionStage = TransitionStage.DISMISSED
                    self.hidden = true
                    self.fadeIn()
                    return Void()
                }
            )
        }
    }

}
