//
//  WGMNavigationUtils.swift
//  WegmannMobile
//
//  Created by Steven T. Norris on 9/5/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

import UIKit

class FAUNavigationUtils{
    
    //Pops a navigation controller back to the first instance of the class given
    class func popToFirstInstance(navigationController:UINavigationController, viewControllerType:UIViewController.Type, animated:Bool) -> Bool
    {
        var controller:UIViewController
        var i=navigationController.viewControllers.count-1
        while i>=0
        {
            controller = navigationController.viewControllers[i] as UIViewController
            if controller.isKindOfClass(viewControllerType){
                navigationController.popToViewController(controller, animated: animated)
                return true
            }
             i -= 1
        }
        return false
    }
    
    //Pops a navigations controller back to the first instance of the classes given
    class func popToFirstInstance(navigationController:UINavigationController, viewControllerTypes:[UIViewController.Type], animated:Bool) -> Bool
    {
        var controller:UIViewController
        var i=navigationController.viewControllers.count-1
        while i>=0
        {
            controller = navigationController.viewControllers[i] as UIViewController
            for controllerType in viewControllerTypes{
                if(controller.isKindOfClass(controllerType)){
                    navigationController.popToViewController(controller, animated: animated)
                    return true
                }
            }
            
            i -= 1
        }
        return false
    }
    
    //Fade in view as subview of root view
    class func fadeInTempView(rootView:UIView, fadeView:UIView, fadeBackground:UIColor, delay:NSTimeInterval, duration:NSTimeInterval, completion:(Bool) -> Void){
        fadeView.alpha = 0.0
        rootView.addSubview(fadeView)
        fadeView.frame = CGRectMake(0, 0, rootView.frame.size.width, rootView.frame.size.height)
        UIView.animateWithDuration(
            duration,
            delay: delay,
            options: [],
            animations: {
                fadeView.alpha = 1.0
                fadeView.backgroundColor = fadeBackground
            },
            completion: completion)
    }
    
    //Fade out view as subview of root view
    class func fadeOutTempView(rootView:UIView, fadeView:UIView, delay:NSTimeInterval, duration:NSTimeInterval, completion:(Bool) -> Void){
        UIView.animateWithDuration(
            duration,
            delay: delay,
            options: [],
            animations: {
                fadeView.alpha = 0.0
            },
            completion: {finished in
                fadeView.removeFromSuperview()
                completion(finished)
            })
    }
    
    //Fades in a view for a short time, then fades out
    class func fadeInOutDelay(fadeView:UIView, delay:NSTimeInterval, duration:NSTimeInterval, visibleDurationSec:UInt64, completionIn:(Bool) -> Void, completionOut:(Bool) -> Void)
    {
        fadeView.hidden = false
        UIView.animateWithDuration(
            duration,
            delay: delay,
            options: [],
            animations: {
                fadeView.alpha = 1.0
                return Void()
            },
            completion: {finished in
                completionIn(finished)
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(visibleDurationSec * NSEC_PER_SEC)), dispatch_get_main_queue(), {
                    UIView.animateWithDuration(
                        duration,
                        delay: delay,
                        options: [],
                        animations: {
                            fadeView.alpha = 0.0
                            return Void()
                        },
                        completion: {finished in
                            completionOut(finished)
                            fadeView.hidden = true
                            return Void()
                        })
                })
                return Void()
            })
    }
}