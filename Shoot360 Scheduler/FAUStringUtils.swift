//
//  FAUStringUtils.swift
//  WegmannMobile
//
//  Created by Steven T. Norris on 10/10/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

import Foundation

extension String{
    
    //Gets last index of a character
    func lastIndexOf(char:Character) -> String.Index?{
        var foundChar:Character? = nil
        var foundIndex:String.Index? = nil
        var index = self.endIndex
        while index != self.startIndex
        {
            foundChar = self[index]
            if(foundChar == char){
                foundIndex = index
                break;
            }
            index = index.predecessor()
        }
        if(foundIndex == nil){
            foundChar = self[self.startIndex]
            if(foundChar == char){
                foundIndex = self.startIndex
            }
        }
        return foundIndex
    }
    
    
}

//Regex operator

infix operator =~ { associativity left precedence 160 }

func =~(string:String, pattern:String) -> Bool {
    
    let options: NSRegularExpressionOptions =
        [NSRegularExpressionOptions.DotMatchesLineSeparators]

    
    return regexTry(string, pattern: pattern, options: options)
}

//Case insensitive regex operator

infix operator =~~ { associativity left precedence 160 }

func =~~(string:String, pattern:String) -> Bool {
    let options: NSRegularExpressionOptions =
        [NSRegularExpressionOptions.DotMatchesLineSeparators, NSRegularExpressionOptions.CaseInsensitive]
    
    
    return regexTry(string, pattern: pattern, options: options)
}

//Regex helper method

func regexTry(string:String, pattern:String, options:NSRegularExpressionOptions) -> Bool{
    let regex = try! NSRegularExpression(pattern: pattern, options: options)
    
    var matches = 0
    matches = regex.numberOfMatchesInString(string,
                                            options: [],
                                            range: NSMakeRange(0, string.characters.count))
    
    return matches > 0
}