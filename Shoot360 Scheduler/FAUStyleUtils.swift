
//
//  FAUStyleUtils.swift
//  WegmannMobile
//
//  Created by Steven T. Norris on 10/24/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
    //Initializes a UIColor based off of a hex string
    convenience init(hexString:String, alpha:CGFloat){
        var cleanHex:String!
        //Eliminate common prefixes
        if(hexString.hasPrefix("#")){
            cleanHex = hexString.substringFromIndex(hexString.startIndex.advancedBy(1))
        }
        else if(hexString.hasPrefix("0x")){
            cleanHex = hexString.substringFromIndex(hexString.startIndex.advancedBy(2))
        }
        else{
            cleanHex = hexString
        }
        
        //Check for correct length
        if(hexString.characters.count != 6){
            self.init(red:0/255.0,green:0/255.0,blue:0/255.0, alpha: alpha)
        }
        else{
            let rString = cleanHex.substringToIndex(cleanHex.startIndex.advancedBy(2))
            var gString = cleanHex.substringFromIndex(cleanHex.startIndex.advancedBy(2))
            gString = gString.substringToIndex(gString.startIndex.advancedBy(2))
            var bString = cleanHex.substringFromIndex(cleanHex.startIndex.advancedBy(4))
            bString = bString.substringToIndex(bString.startIndex.advancedBy(2))
            
            var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0
            NSScanner(string: rString).scanHexInt(&r)
            NSScanner(string: gString).scanHexInt(&g)
            NSScanner(string: bString).scanHexInt(&b)
            
            self.init(red:CGFloat(r)/255.0,green:CGFloat(g)/255,blue:CGFloat(b)/255,alpha:alpha)
        }
    }
}

class FAUStyleUtils {

    //Calculates luminosity differential to determine whether color is a good match or not
    class func luminosityDifference(primaryColor:(r:CGFloat, g:CGFloat, b:CGFloat), secondaryColor:(r:CGFloat, g:CGFloat, b:CGFloat)) -> CGFloat{
        
        //Get base number for first color
        let basePrimary =   0.2126 * pow(primaryColor.r,2.2) +
                            0.7152 * pow(primaryColor.g,2.2) +
                            0.0722 * pow(primaryColor.b,2.2) +
                            0.05
        
        //Get base number for second color
        let baseSecondary = 0.2126 * pow(secondaryColor.r,2.2) +
                            0.7152 * pow(secondaryColor.g,2.2) +
                            0.0722 * pow(secondaryColor.b,2.2) +
                            0.05
        
        //Return difference
        if(basePrimary > baseSecondary){
            return basePrimary/baseSecondary
        }
        else{
            return baseSecondary/basePrimary
        }
    }
    
    //Adds border to only the approved sides using views for autoresizing
    class func addBorders(rootView:UIView, edges:[UIRectEdge], borderWidth:CGFloat, borderRadius:CGFloat, borderColor:UIColor){
        
        if edges.contains(UIRectEdge.Top) || edges.contains(UIRectEdge.All){
            let border = UIView()
            border.backgroundColor = borderColor;
            border.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleBottomMargin]
            border.frame = CGRectMake(0, 0, rootView.frame.size.width, borderWidth);
            border.layer.cornerRadius = borderRadius
            border.layer.masksToBounds = true
            rootView.addSubview(border);
        }
    }
}