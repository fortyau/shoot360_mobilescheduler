//
//  FAUTabBarSplashViewController.swift
//  
//  Provides tab bar functionality with a splash screen displayed first and no tab selected
//
//  Created by Steven T. Norris on 9/16/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

import UIKit

class FAUTabBarSplashViewController: UIViewController, UITabBarDelegate {

//----------------
//MARK: Variables
//----------------
    
    //Variables
    var splashViewController:UIViewController!
    var displayView:UIView!;
    var controllers:Dictionary<Int,UIViewController>!
    var tabBar:UITabBar!
    var currViewController:UIViewController? = nil
    var transitionAnimated:Bool = true
    var animationDuration:NSTimeInterval = 0.5
    var animationOptions:UIViewAnimationOptions = UIViewAnimationOptions.TransitionCrossDissolve
    var animationDelay:NSTimeInterval = 0.0
    var animationCompletion:((Bool) -> Void)!

//-----------------
//MARK: Initialization
//-----------------
    
    //Initializes controller
    init(splashViewController:UIViewController, tabBarHeight:Int = 50, transitionAnimated:Bool = true){
        super.init(nibName: nil, bundle: nil)
        
       self.controllers = Dictionary<Int,UIViewController>()
        
        //Set animation and completion
        self.transitionAnimated = transitionAnimated
        self.animationCompletion = {finished in return Void()}
        
        //Set splash view
        self.splashViewController = splashViewController
        self.splashViewController.view.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleBottomMargin, UIViewAutoresizing.FlexibleTopMargin]
        
        //Create main view
        self.view = UIView(frame: UIScreen.mainScreen().bounds)
        
        //Setup tab bar
        tabBar = UITabBar(frame: CGRectMake(0, UIScreen.mainScreen().bounds.height - CGFloat(tabBarHeight), UIScreen.mainScreen().bounds.width, CGFloat(tabBarHeight)))
        let items = [UITabBarItem]()
        self.tabBar.setItems(items, animated:false)
        self.tabBar.delegate = self
        
        //Setup view for displaying selection
        displayView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height - CGFloat(tabBar.bounds.height)))
        displayView.autoresizesSubviews = true
        
        //Add Views
        self.view.addSubview(displayView)
        self.view.addSubview(tabBar)
        
        //Initialize tabbar as hidden and display splash screen
        self.hideTabBar(false)
        self.displaySplash(false)
    }

    //Inits via nib name
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    //Required via UIViewController specs
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

//--------------
//MARK: Event Handlers
//--------------
   
    //Handles all initialization on load
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //Handles memory warnings
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//--------------
//MARK: Actions
//--------------
    
    //Hides tab bar with slide down animation. If animated is false, all other params are unused
    func hideTabBar(animated:Bool, duration:NSTimeInterval = 0.5, delay:NSTimeInterval = 0, options:UIViewAnimationOptions = UIViewAnimationOptions.CurveLinear, completion:((Bool) -> Void) = {finished in return Void()}){
        if(animated){
            UIView.animateWithDuration(duration, delay: delay, options: options, animations: {
                    self.adjustTabBarHidden()
                    self.adjustDisplayViewTabDisplayed()
                    self.displayView.layoutIfNeeded()
                }, completion: {finished in
                    self.tabBar.hidden = true
                    completion(finished)
            })
        }
        else{
            self.adjustTabBarHidden()
            self.adjustDisplayViewTabHidden()
        }
    }
    
    //Adjusts frame of tab bar and display view to hide tab bar
    private func adjustTabBarHidden(){
        self.tabBar.frame = CGRectMake(0,UIScreen.mainScreen().bounds.height, self.tabBar.bounds.width, self.tabBar.bounds.height)
    }
    
    //Adjust display view to  match hidden tab bar
    private func adjustDisplayViewTabHidden(){
        self.displayView.frame = CGRectMake(0,0,self.displayView.bounds.width, UIScreen.mainScreen().bounds.height)
    }
    
    //Displays tab bar with slide up animation. If animated is false, all other params are unused
    func displayTabBar(animated:Bool, duration:NSTimeInterval = 0.5, delay:NSTimeInterval = 0, options:UIViewAnimationOptions = UIViewAnimationOptions.CurveLinear, completion:((Bool) -> Void) = {finished in return Void()}){
        if(animated){
            self.tabBar.hidden = false
            UIView.animateWithDuration(duration, delay: delay, options: options, animations: {
                    self.adjustTabBarDisplayed()
                    self.adjustDisplayViewTabDisplayed()
                    self.displayView.layoutIfNeeded()
                }, completion: completion)
        }
        else{
            self.adjustTabBarDisplayed()
            self.adjustDisplayViewTabDisplayed()
        }
        
    }
    
    //Adjusts frame of tab bar to display tab bar
    private func adjustTabBarDisplayed(){
        self.tabBar.frame = CGRectMake(0,UIScreen.mainScreen().bounds.height - self.tabBar.bounds.height, self.tabBar.bounds.width, self.tabBar.bounds.height)
    }
    
    //Adjusts frame of display view to match displayed tab bar
    private func adjustDisplayViewTabDisplayed(){
        self.displayView.frame = CGRectMake(0,0,self.displayView.bounds.width, UIScreen.mainScreen().bounds.height - self.tabBar.bounds.height)
    }
  
    //Displays splash screen. If animated is false, all other params are unused
    func displaySplash(animated:Bool, duration:NSTimeInterval = 0.5, options:UIViewAnimationOptions = UIViewAnimationOptions.TransitionCrossDissolve, completion:((Bool) ->Void) = {finished in return Void()})
    {
        //Prepare for controller transition
        self.splashViewController.viewWillAppear(false)
        self.currViewController?.viewWillDisappear(false)
        self.currViewController?.willMoveToParentViewController(nil)
        
        //Set splash controller with/without animation
        self.splashViewController.view.frame = self.displayView.frame
        if(animated){
            UIView.transitionFromView(self.displayView, toView: self.splashViewController.view, duration: duration, options: options, completion: {finished in
                    self.adjustSplashScreenDisplayed()
                    completion(finished)
                })
        }
        else{
            displayView.removeFromSuperview()
            self.view.addSubview(self.splashViewController.view)
            self.adjustSplashScreenDisplayed()
        }
    }
    
    //Adjusts the splash screen and display view to diplay splash screen
    private func adjustSplashScreenDisplayed()
    {
        //Set new display view
        self.displayView = self.splashViewController.view
        
        //Deselect tab bar
        self.tabBar.selectedItem = nil
        
        //Set up view controller
        self.addChildViewController(splashViewController)
        
        //Set up view
        self.splashViewController.viewDidAppear(false)
        
        //Finish controller add
        self.splashViewController.didMoveToParentViewController(self)
        self.currViewController?.viewDidDisappear(false)
        self.currViewController?.removeFromParentViewController()
        self.currViewController = self.splashViewController
    }

//-------------
//MARK: Datasource
//-------------
    
    //Adds a controller and tab bar item
    func addController(controller:UIViewController, tabBarImage:UIImage?=nil){
        var tag:Int
        let keys:[Int] = [Int](self.controllers.keys)
        repeat{
            tag = Int(arc4random_uniform(999))
        } while(keys.contains(tag))
        controllers[tag] = controller;
        tabBar.items?.append(UITabBarItem(title: controller.title, image: tabBarImage, tag:tag))
    }
    
    //Removes a controller and tab bar item
    func removeController(controller:UIViewController){
        var index = [UIViewController](controllers.values).indexOf(controller)
        let tag = [Int](controllers.keys)[index!]
        controllers.removeValueForKey(tag)
        index = findTabBarItemIndex(tag)
        tabBar.items?.removeAtIndex(index!)
    }

//--------------
//MARK: Utilities
//--------------
    
    //Finds index of tab bar item based on tag
    func findTabBarItemIndex(tag:Int) -> Int{
        var index = 0
        for item:UITabBarItem in tabBar.items! as [UITabBarItem]{
            if(item.tag == tag){
                return index
            }
            index += 1
        }
        return -1
    }
    
//--------------
//MARK: Delegate
//--------------
    
    //Handles selection of a tab bar item
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        
        //Get controller for transition
        let selectedController = self.controllers[item.tag];
        
        //Only transition for new view
        if(self.currViewController! as UIViewController != selectedController!){
            //Prepare for controller transition
            selectedController!.viewWillAppear(false)
            self.currViewController?.viewWillDisappear(false)
            self.currViewController?.willMoveToParentViewController(nil)
            
            //Set new controller with animation
            selectedController!.view.frame = self.displayView.frame
            if(transitionAnimated){
                self.displayView.superview?.addSubview(selectedController!.view)
                UIView.transitionFromView(self.displayView, toView: selectedController!.view, duration: self.animationDuration, options: self.animationOptions, completion: {finished in
                        self.setNewController(selectedController!)
                        self.animationCompletion(finished)
                })
            }
            else{
                self.displayView.removeFromSuperview()
                self.view.addSubview(selectedController!.view)
                setNewController(selectedController!);
            }
        }
        else{
            if(self.currViewController!.isKindOfClass(UINavigationController)){
                (self.childViewControllers[0]as! UINavigationController).popToRootViewControllerAnimated(true)
            }
        }
    }
    
    //Sets up new controller
    private func setNewController(controller:UIViewController){
        //Set new display view
        self.displayView = controller.view
        
        //Set up view controller
        self.addChildViewController(controller)
        
        //Set up view
        controller.viewDidAppear(false)
        
        //Finish controller add
        controller.didMoveToParentViewController(self)
        self.currViewController?.viewDidDisappear(false)
        self.currViewController?.removeFromParentViewController()
        self.currViewController = controller
    }

}
