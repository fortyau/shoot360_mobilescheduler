//
//  FAUValidateUtils.swift
//  WegmannMobile
//
//  Created by Steven T. Norris on 9/29/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

import Foundation

class FAUValidateUtils{
    
    //Validates whether a string is a correctly formatted email
     class func isValidEmail(email:String) -> Bool{
        // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
        //Stricter filter
        let filter = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        //Lax filter
        //var filter = ".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*"
        var matches:[NSTextCheckingResult]
        do{
            let emailTest = try NSRegularExpression(pattern: filter, options: NSRegularExpressionOptions.CaseInsensitive)
            matches = emailTest.matchesInString(email, options: [], range: NSMakeRange(0,email.characters.count))
        }
        catch {
            return false
        }
        return (matches.count>0)
    }
}