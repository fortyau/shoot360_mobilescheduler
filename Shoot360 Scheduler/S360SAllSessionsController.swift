//
//  S360SAllSessionsController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/19/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit
import FSCalendar
import SCLAlertView

class S360SAllSessionsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var eventsView:UIView!
    @IBOutlet var sessionsTbl:UITableView!
    @IBOutlet var locationLbl:UILabel!
    @IBOutlet var errorLbl:FAUErrorLabel!
    
    var eventTypes = ["Ball Handling", "Shooting", "Virtual Training"]
    var selectedBtn:S360SEventTypeButton?
    
    //TODO: THIS IS JUST DEMO CODE
    var dateFormatter = NSDateFormatter()
    var sessions:[String:[[String:AnyObject]]] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        //Set up navigation
        self.title = "All Sessions"
        self.edgesForExtendedLayout = UIRectEdge.None
        
        //Setup event type buttons
        self.setupEventTypeBtns()
        
        //Setup navigation items
        let nav = self.navigationController as! S360SNavigationController
        nav.addNavSwitchButton(self, state: S360SNavigationController.SEARCH)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        //Setup location
        locationLbl.text = "Raccoon City - 07/08/2016"
        
        //Set up error label
        view.bringSubviewToFront(errorLbl)
        
        //TODO: THIS IS JUST DEMO CODE
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        sessions = [
            "Ball Handling":[
                    [
                        "location":"Narnia",
                        "court":1,
                        "startDate":dateFormatter.dateFromString("2016-05-23 13:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 14:00:00")!
                    ],
                    [
                        "location":"Narnia",
                        "court":1,
                        "startDate":dateFormatter.dateFromString("2016-05-23 15:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 16:00:00")!
                    ],
                    [
                        "location":"Narnia",
                        "court":1,
                        "startDate":dateFormatter.dateFromString("2016-05-23 17:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 18:00:00")!
                    ],
                    [
                        "location":"Narnia",
                        "court":1,
                        "startDate":dateFormatter.dateFromString("2016-05-23 18:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 19:00:00")!
                    ]
                ],
            "Shooting":[
                    [
                        "location":"Narnia",
                        "court":2,
                        "startDate":dateFormatter.dateFromString("2016-05-23 13:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 14:00:00")!
                    ],
                    [
                        "location":"Narnia",
                        "court":2,
                        "startDate":dateFormatter.dateFromString("2016-05-23 15:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 16:00:00")!
                    ],
                    [
                        "location":"Narnia",
                        "court":2,
                        "startDate":dateFormatter.dateFromString("2016-05-23 17:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 18:00:00")!
                    ],
                    [
                        "location":"Narnia",
                        "court":2,
                        "startDate":dateFormatter.dateFromString("2016-05-23 18:00:00")!,
                        "endDate":dateFormatter.dateFromString("2016-05-23 19:00:00")!
                    ]
                ],
            "Virtual Training":[
                    [
                    "location":"Narnia",
                    "court":3,
                    "startDate":dateFormatter.dateFromString("2016-05-23 13:00:00")!,
                    "endDate":dateFormatter.dateFromString("2016-05-23 14:00:00")!
                    ],
                    [
                    "location":"Narnia",
                    "court":5,
                    "startDate":dateFormatter.dateFromString("2016-05-23 13:00:00")!,
                    "endDate":dateFormatter.dateFromString("2016-05-23 14:00:00")!
                    ],
                    [
                    "location":"Narnia",
                    "court":3,
                    "startDate":dateFormatter.dateFromString("2016-05-23 17:00:00")!,
                    "endDate":dateFormatter.dateFromString("2016-05-23 18:00:00")!
                    ],
                    [
                    "location":"Narnia",
                    "court":5,
                    "startDate":dateFormatter.dateFromString("2016-05-23 18:00:00")!,
                    "endDate":dateFormatter.dateFromString("2016-05-23 19:00:00")!
                    ]
                ]

        ]
        
        //Setup date formatter
        dateFormatter.dateFormat = "hh:mm a"
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Styling
        sessionsTbl.rowHeight = UITableViewAutomaticDimension
        sessionsTbl.estimatedRowHeight = 100.0
        sessionsTbl.separatorColor = Colors.REALLIGHTGREY
        sessionsTbl.separatorInset = UIEdgeInsetsZero
        
        let sessionsTblBorderPath = UIBezierPath(roundedRect: sessionsTbl.bounds, byRoundingCorners: [UIRectCorner.BottomRight, UIRectCorner.BottomLeft, UIRectCorner.TopRight], cornerRadii: CGSizeMake(Numbers.CORNERRADIUS, Numbers.CORNERRADIUS))
        let sessionsTblBorder = CAShapeLayer()
        sessionsTblBorder.frame = sessionsTbl.bounds
        sessionsTblBorder.path = sessionsTblBorderPath.CGPath
        sessionsTblBorder.lineWidth = Numbers.BORDERREG
        sessionsTblBorder.strokeColor = Colors.REALLIGHTGREY.CGColor
        sessionsTblBorder.fillColor = UIColor.clearColor().CGColor
        sessionsTbl.layer.addSublayer(sessionsTblBorder)
        
        sessionsTbl.bounces = false
        
        self.view.bringSubviewToFront(eventsView)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Setup calls
    func setupEventTypeBtns(){
        var x = 0
        var index = 0
        for eventType in eventTypes {
            x = 50 * index + 2 * index
            let button = S360SEventTypeButton(frame: CGRect(x: x, y: 0, width: Int(eventsView.frame.height), height: Int(eventsView.frame.height)), eventType: eventType)
            
            button.addTarget(self, action: #selector(S360SAllSessionsController.eventTypeBtnTouch), forControlEvents: UIControlEvents.TouchUpInside)
            eventsView.addSubview(button)
            if index == 0{
                selectedBtn = button
                button.selected = true
            }
            index = index + 1
        }
        
    }
    
    //Button Actions
    func eventTypeBtnTouch(sender:S360SEventTypeButton){
        if selectedBtn != sender {
            if selectedBtn != nil {
                selectedBtn!.selected = false
            }
            sender.selected = true
            selectedBtn = sender
        }
    }
    
    //Table Datasource/Delegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:S360SMiniSessionTableCell? = tableView.dequeueReusableCellWithIdentifier(XIBFiles.MINISESSIONTABLECELL) as? S360SMiniSessionTableCell
        
        if ((cell == nil)){
            tableView.registerNib(UINib(nibName: XIBFiles.MINISESSIONTABLECELL, bundle: nil), forCellReuseIdentifier: XIBFiles.MINISESSIONTABLECELL)
            cell = tableView.dequeueReusableCellWithIdentifier(XIBFiles.MINISESSIONTABLECELL) as? S360SMiniSessionTableCell
        }
        
        var session = sessions[selectedBtn!.eventType!]![indexPath.row]
        cell!.courtsLbl.text = String(session["court"]!)
        cell!.startTimeLbl.text = dateFormatter.stringFromDate(session["startDate"] as! NSDate)
        cell!.endTimeLbl.text = dateFormatter.stringFromDate(session["endDate"] as! NSDate)
        cell!.separatorInset = UIEdgeInsetsZero
        cell!.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessions[selectedBtn!.eventType!]!.count
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var session = sessions[selectedBtn!.eventType!]![indexPath.row]
        
        //This is split up because XCode can't handle compiling this because it is 'too complex', which is rather silly, but just the way things are.
        var descript = "\n" + selectedBtn!.eventType!
        descript = descript + " - " + dateFormatter.stringFromDate(session["startDate"]! as! NSDate)
        descript = descript + " to " + dateFormatter.stringFromDate(session["endDate"]! as! NSDate) + "\n"
        
        //        let alertAppearance = SCLAlertView.SCLAppearance(
        //            kDefaultShadowOpacity: <#T##CGFloat#>,
        //            kCircleTopPosition: <#T##CGFloat#>,
        //            kCircleBackgroundTopPosition: <#T##CGFloat#>,
        //            kCircleHeight: <#T##CGFloat#>,
        //            kCircleIconHeight: <#T##CGFloat#>,
        //            kTitleTop: <#T##CGFloat#>,
        //            kTitleHeight: <#T##CGFloat#>,
        //            kWindowWidth: <#T##CGFloat#>,
        //            kWindowHeight: <#T##CGFloat#>,
        //            kTextHeight: <#T##CGFloat#>,
        //            kTextFieldHeight: <#T##CGFloat#>,
        //            kTextViewdHeight: <#T##CGFloat#>,
        //            kButtonHeight: <#T##CGFloat#>,
        //            kTitleFont: <#T##UIFont#>,
        //            kTextFont: <#T##UIFont#>,
        //            kButtonFont: <#T##UIFont#>,
        //            showCloseButton: <#T##Bool#>,
        //            showCircularIcon: <#T##Bool#>,
        //            shouldAutoDismiss: <#T##Bool#>,
        //            contentViewCornerRadius: <#T##CGFloat#>,
        //            fieldCornerRadius: <#T##CGFloat#>,
        //            buttonCornerRadius: <#T##CGFloat#>,
        //            hideWhenBackgroundViewIsTapped: <#T##Bool#>,
        //            contentViewColor: <#T##UIColor#>,
        //            contentViewBorderColor: <#T##UIColor#>,
        //            titleColor: <#T##UIColor#>
        //        )
        
        let alertAppearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            kTextFont: Fonts.BODY,
            kTitleFont: Fonts.TITLEMID,
            kButtonFont: Fonts.BUTTON,
            buttonCornerRadius: Numbers.CORNERRADIUS,
            kCircleIconHeight: Numbers.ALERTICONHEIGHT,
            kCircleHeight: Numbers.ALERTCIRCLEHEIGHT
        )
        
        let scheduleAlert = SCLAlertView(appearance: alertAppearance)
        scheduleAlert.addButton("Schedule", action: {
            self.errorLbl.displayMessage(FAUErrorLabel.MessageLevel.INFO, message: "Sessions Scheduled")
        })
        scheduleAlert.addButton("Cancel", action: {})
        scheduleAlert.showTitle("Fillory - 09/23/2016", subTitle: descript, style: .Info, circleIconImage: Images.LOGOSOLO)
    }

    

}
