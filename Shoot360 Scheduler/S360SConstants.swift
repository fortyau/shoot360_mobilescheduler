//
//  S360SConstants.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/17/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome

struct XIBFiles {
    static var LOGINVIEW                    = "S360SLoginView"
    static var ALLSESSIONSVIEW              = "S360SAllSessionsView"
    static var SESSIONVIEW                  = "S360SSessionView"
    static var LOCATIONVIEW                 = "S360SLocationView"
    static var MYSCHEDULEVIEW               = "S360SMyScheduleView"
    static var FINDSESSIONVIEW              = "S360SFindSessionView"
    static var SESSIONSMATCHVIEW            = "S360SSessionsMatchView"
    
    static var SCHEDULEDEVENTVIEW           = "S360SScheduledEventView"
    static var SCHEDULEDBREAKVIEW           = "S360SScheduledBreakView"
    
    static var SESSIONTABLECELL             = "S360SSessionTableCell"
    static var MINISESSIONTABLECELL         = "S360SMiniSessionTableCell"
    static var SESSIONEVENTTYPETABLECELL    = "S360SSessionEventTypeTableCell"
    static var BREAKEVENTTYPETABLECELL      = "S360SBreakEventTypeTableCell"
    static var SESSIONMATCHTABLECELL        = "S360SSessionMatchTableCell"
}

struct Numbers {
    static var CORNERRADIUS         = CGFloat(5.0)
    static var BORDERREG            = CGFloat(2.0)
    static var QUICKDURATION        = NSTimeInterval(0.25)
    static var BUTTONHEIGHT         = CGFloat(60.0)
    static var ALERTICONHEIGHT      = CGFloat(45.0)
    static var ALERTCIRCLEHEIGHT    = CGFloat(0.0)
}

struct Colors {
    static var FADEBACKGROUND   = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.7)
    static var REALLIGHTGREY    = UIColor(hexString: "d5d5d5", alpha: 1)
    static var REALDARKGREY     = UIColor(colorLiteralRed: 100/255, green: 100/255, blue: 100/255, alpha: 1.0)
    static var SHOOT360RED      = UIColor(hexString: "ed2224", alpha: 1)
}

struct Fonts {
    static var SPINNER      = UIFont(name: "HelveticaNeue", size: 30)!
    static var SMALLBUTTON  = UIFont(name: "HelveticaNeue", size: 16)!
    static var NAVBARICONS  = UIFont(name: "FontAwesome", size: 25)!
    static var BODY         = UIFont(name: "HelveticaNeue", size: 12)!
    static var TITLEMID     = UIFont(name: "HelveticaNeue-Bold", size: 20)!
    static var TITLESMALL     = UIFont(name: "HelveticaNeue-Bold", size: 14)!
    static var BUTTON       = UIFont(name: "HelveticaNeue-Bold", size: 20)!
}

struct Icons {
    static var PROFILE  = NSString.fontAwesomeIconStringForEnum(FAIcon.FAUser)
    static var SEARCH   = NSString.fontAwesomeIconStringForEnum(FAIcon.FASearch)
}

struct Images {
    static var LOGOSOLO     = UIImage(named:"SH360T-LOGO_solo.png")
    static var LOGOSOLORED  = UIImage(named:"SH360T-LOGO_solo_red.png")
    static var LOGOSOLOWHITE  = UIImage(named:"SH360T-LOGO_solo_white.png")
    
    static var SHOOTING     = UIImage(named: "shooting.png")
    static var BALLHANDLING = UIImage(named: "ballhandling.png")
    static var TRAINING     = UIImage(named: "training.png")
    static var BREAK        = UIImage(named: "break.png")
    static var DEFAULT      = UIImage(named: "default.png")
    
    static var STAR         = UIImage(named: "star.png")
    
    static func getEventImage(eventType:S360SEvent.S360SEventType) -> UIImage{
        switch eventType{
            case S360SEvent.S360SEventType.BallHandling:
                return Images.BALLHANDLING!
            case S360SEvent.S360SEventType.Shooting:
                return Images.SHOOTING!
            case S360SEvent.S360SEventType.Train360:
                return Images.TRAINING!
            case S360SEvent.S360SEventType.Break:
                return Images.BREAK!
            case S360SEvent.S360SEventType.Default:
                return Images.DEFAULT!
        }
    }

    static func getEventImage(eventTypeS:String) -> UIImage{
        let eventType = S360SEvent.parseEventType(eventTypeS)
        return getEventImage(eventType)
    }
}
