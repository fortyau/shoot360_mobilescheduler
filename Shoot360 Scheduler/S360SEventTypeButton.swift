//
//  S360SEventTypeButton.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/19/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit


class S360SEventTypeButton: UIButton {

    var eventType:String? = nil
    
    override var selected: Bool {
        willSet {
            super.selected = newValue
            if newValue {
                UIView.animateWithDuration(Numbers.QUICKDURATION, animations: { () -> Void in
                    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + Numbers.BORDERREG, self.frame.size.width, self.frame.size.height)
                })
            }
            else {
                UIView.animateWithDuration(Numbers.QUICKDURATION, animations: { () -> Void in
                    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y - Numbers.BORDERREG, self.frame.size.width, self.frame.size.height)
                })
            }
        }
    }
    
    init(frame:CGRect, eventType:String){
        let newFrame = CGRectMake(frame.origin.x, frame.origin.y + Numbers.BORDERREG, frame.size.height, frame.size.width)
        super.init(frame: newFrame)
        
        self.eventType = eventType
        self.setImage(Images.getEventImage(eventType), forState: UIControlState.Normal)
        
        self.backgroundColor = UIColor.whiteColor()
        
        let btnBorderPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [UIRectCorner.TopRight, UIRectCorner.TopLeft], cornerRadii: CGSizeMake(Numbers.CORNERRADIUS, Numbers.CORNERRADIUS))
        let btnBorder = CAShapeLayer()
        btnBorder.frame = self.bounds
        btnBorder.path = btnBorderPath.CGPath
        btnBorder.lineWidth = Numbers.BORDERREG
        btnBorder.strokeColor = Colors.REALLIGHTGREY.CGColor
        btnBorder.fillColor = UIColor.clearColor().CGColor
        self.layer.addSublayer(btnBorder)
        
        let mask = UIView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - Numbers.BORDERREG))
        mask.backgroundColor = UIColor.blackColor()
        self.layer.mask = mask.layer

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    
    


}
