//
//  S360SEventTypeCell.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 7/6/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

struct S360SEvent {
    
    enum S360SEventType: String {
        case Break = "Break"
        case BallHandling = "Ball Handling"
        case Shooting = "Shooting"
        case Train360 = "Train360"
        case Default = "Session"
    }
    
    static func parseEventType(eventTypeS: String) -> S360SEventType {
        if eventTypeS =~~ "ball handling|ballhandlng|ball handler|ballhandler" {
            return S360SEventType.BallHandling
        }
        else if eventTypeS =~~ "shooting|shooter"{
            return S360SEventType.Shooting
        }
        else if eventTypeS =~~ "personal training|virtual training|personal train|virtual train|train360|train 360"{
            return S360SEventType.Train360
        }
        else if eventTypeS =~~ "break"{
            return S360SEventType.Break
        }
        else{
            return S360SEventType.Default
        }

    }
    
}

protocol S360SEventTypeView {
    
    func getValue() -> Int
    func getType() -> S360SEvent.S360SEventType
    
}
