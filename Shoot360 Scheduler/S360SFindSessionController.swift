//
//  S360SFindSessionController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 6/6/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit
import FSCalendar
import SCLAlertView

class S360SFindSessionController: UIViewController, UITableViewDelegate, UITableViewDataSource, FSCalendarDelegate {

    @IBOutlet var dateCal:FSCalendar!
    @IBOutlet var startTimeField:UITextField!
    @IBOutlet var eventsTbl:UITableView!
    @IBOutlet var findBtn:UIButton!
    @IBOutlet var dateCalHeightCnst:NSLayoutConstraint!
    @IBOutlet var startTimeLbl:UILabel!
    @IBOutlet var errorLbl:FAUErrorLabel!
    @IBOutlet var addBtn:UIButton!
    
    var timeFormatter = NSDateFormatter()
    var sessions:Array<NSMutableDictionary> = []
    var datePicker:UIDatePicker = UIDatePicker()
    var defaultTime = "10:00 AM"
    var dateField = UITextField()
    var dateLbl = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup table cells
        eventsTbl.registerNib(UINib(nibName: XIBFiles.SESSIONEVENTTYPETABLECELL, bundle: nil), forCellReuseIdentifier: XIBFiles.SESSIONEVENTTYPETABLECELL)
        eventsTbl.registerNib(UINib(nibName: XIBFiles.BREAKEVENTTYPETABLECELL, bundle: nil), forCellReuseIdentifier: XIBFiles.BREAKEVENTTYPETABLECELL)
        
        //Setup helpers
        timeFormatter.dateStyle = NSDateFormatterStyle.NoStyle
        timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        //Set up navigation
        self.title = "Find Session"
        self.edgesForExtendedLayout = UIRectEdge.None
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        //Setup navigation items
        let nav = self.navigationController as! S360SNavigationController
        nav.addNavSwitchButton(self, state: S360SNavigationController.SEARCH)
        
        //Setup pickers
        datePicker.datePickerMode = UIDatePickerMode.Time
        datePicker.addTarget(self, action: #selector(S360SFindSessionController.updateStartTime), forControlEvents: UIControlEvents.ValueChanged)
        startTimeField.inputView = datePicker
        datePicker.minuteInterval = 5
        
        let toolbar:UIToolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.BlackTranslucent
        let doneButton = UIButton()
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.addTarget(self, action: #selector(S360SFindSessionController.timeDoneTouch), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.titleLabel!.font = Fonts.SMALLBUTTON
        doneButton.titleLabel!.textColor = UIColor.whiteColor()
        doneButton.frame = CGRectMake(0.0, 0.0, 100, 30)
        doneButton.backgroundColor = UIColor.blackColor()
        doneButton.layer.cornerRadius = Numbers.CORNERRADIUS
        let doneBarButton:UIBarButtonItem = UIBarButtonItem(customView: doneButton)
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target:nil, action:nil),doneBarButton], animated: false)
        toolbar.sizeToFit()
        toolbar.userInteractionEnabled = true
        startTimeField.inputAccessoryView = toolbar
        
        startTimeField.text = self.defaultTime
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Styling
        eventsTbl.rowHeight = 80.0
        eventsTbl.estimatedRowHeight = 80.0
        
        eventsTbl.setEditing(true, animated: true)
        
        startTimeField.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        startTimeField.layer.borderWidth = Numbers.BORDERREG
        startTimeField.layer.cornerRadius = Numbers.CORNERRADIUS
        startTimeField.tintColor = Colors.SHOOT360RED
        
        datePicker.backgroundColor = Colors.REALDARKGREY
        datePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        
        findBtn.layer.cornerRadius = Numbers.CORNERRADIUS

        //TODO: Rounded borders on the bottom of the table
//        let eventsTblBorderPath = UIBezierPath(roundedRect: eventsTbl.bounds, byRoundingCorners: [UIRectCorner.BottomRight, UIRectCorner.BottomLeft], cornerRadii: CGSizeMake(Numbers.CORNERRADIUS, Numbers.CORNERRADIUS))
//        let eventsTblBorder = CAShapeLayer()
//        eventsTblBorder.frame = eventsTbl.bounds
//        eventsTblBorder.path = eventsTblBorderPath.CGPath
//        eventsTblBorder.lineWidth = Numbers.BORDERREG
//        eventsTblBorder.strokeColor = Colors.REALLIGHTGREY.CGColor
//        eventsTblBorder.fillColor = UIColor.clearColor().CGColor
//        self.view.layer.addSublayer(eventsTblBorder)
        
        eventsTbl.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        eventsTbl.layer.borderWidth = 2
        
        let addBtnPath = CAShapeLayer()
        addBtnPath.path = UIBezierPath(roundedRect: eventsTbl.bounds, byRoundingCorners: [UIRectCorner.TopRight, UIRectCorner.TopLeft], cornerRadii: CGSizeMake(Numbers.CORNERRADIUS, Numbers.CORNERRADIUS)).CGPath
        addBtn.layer.mask = addBtnPath

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Helpers
    func updateStartTime(){
        self.startTimeField.text = self.timeFormatter.stringFromDate(self.datePicker.date)
    }
    
    func addEventToSessions(title:String){
        sessions.append(["title":title, "minutes":30])
        eventsTbl.reloadData()
        eventsTbl.scrollToRowAtIndexPath(NSIndexPath(forRow: sessions.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
    }
    
    //Button Actions
    func timeDoneTouch(sender:AnyObject){
        self.updateStartTime()
        startTimeField.resignFirstResponder()
    }
    
    @IBAction func addEvent(){
        //        let alertAppearance = SCLAlertView.SCLAppearance(
        //            kDefaultShadowOpacity: <#T##CGFloat#>,
        //            kCircleTopPosition: <#T##CGFloat#>,
        //            kCircleBackgroundTopPosition: <#T##CGFloat#>,
        //            kCircleHeight: <#T##CGFloat#>,
        //            kCircleIconHeight: <#T##CGFloat#>,
        //            kTitleTop: <#T##CGFloat#>,
        //            kTitleHeight: <#T##CGFloat#>,
        //            kWindowWidth: <#T##CGFloat#>,
        //            kWindowHeight: <#T##CGFloat#>,
        //            kTextHeight: <#T##CGFloat#>,
        //            kTextFieldHeight: <#T##CGFloat#>,
        //            kTextViewdHeight: <#T##CGFloat#>,
        //            kButtonHeight: <#T##CGFloat#>,
        //            kTitleFont: <#T##UIFont#>,
        //            kTextFont: <#T##UIFont#>,
        //            kButtonFont: <#T##UIFont#>,
        //            showCloseButton: <#T##Bool#>,
        //            showCircularIcon: <#T##Bool#>,
        //            shouldAutoDismiss: <#T##Bool#>,
        //            contentViewCornerRadius: <#T##CGFloat#>,
        //            fieldCornerRadius: <#T##CGFloat#>,
        //            buttonCornerRadius: <#T##CGFloat#>,
        //            hideWhenBackgroundViewIsTapped: <#T##Bool#>,
        //            contentViewColor: <#T##UIColor#>,
        //            contentViewBorderColor: <#T##UIColor#>,
        //            titleColor: <#T##UIColor#>
        //        )
        
        let alertAppearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            kTextFont: Fonts.BODY,
            kTitleFont: Fonts.TITLEMID,
            kButtonFont: Fonts.BUTTON,
            buttonCornerRadius: Numbers.CORNERRADIUS,
            kCircleIconHeight: Numbers.ALERTICONHEIGHT,
            kCircleHeight: Numbers.ALERTCIRCLEHEIGHT
        )
        
        let addAlert = SCLAlertView(appearance: alertAppearance)
        addAlert.addButton("Ball Handling", action: {self.addEventToSessions("Ball Handling")})
        addAlert.addButton("Shooting", action: {self.addEventToSessions("Shooting")})
        addAlert.addButton("Train360", action: {self.addEventToSessions("Train360")})
        addAlert.addButton("Break", action: {self.addEventToSessions("Break")})
        addAlert.addButton("Cancel", action: {})
        
        addAlert.showTitle("Select an Event", subTitle: "", style: .Info, circleIconImage: Images.LOGOSOLO)

    }
    
    @IBAction func findBtnTouch(){
        let sessionMatch = S360SSessionsMatchController(nibName: XIBFiles.SESSIONSMATCHVIEW, bundle: nil)
        self.navigationController?.pushViewController(sessionMatch, animated: true)
    }
    
    //Table Delegate/Datasource
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        let object = self.sessions[sourceIndexPath.row]
        sessions.removeAtIndex(sourceIndexPath.row)
        sessions.insert(object, atIndex: destinationIndexPath.row)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    func tableView(tableView: UITableView, targetIndexPathForMoveFromRowAtIndexPath sourceIndexPath: NSIndexPath, toProposedIndexPath proposedDestinationIndexPath: NSIndexPath) -> NSIndexPath {
        return proposedDestinationIndexPath
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            sessions.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //Prep session and event type
        let session = sessions[indexPath.row]
        let eventType = S360SEvent.parseEventType(session["title"] as! String)
        
        //Dequeue cell and setup
        let sessionCell = tableView.dequeueReusableCellWithIdentifier(XIBFiles.SESSIONEVENTTYPETABLECELL) as! S360SSessionEventTypeTableCell
        sessionCell.eventType = eventType
        sessionCell.session = session
        sessionCell.timeField.text = String(session["minutes"] as! Int)
        sessionCell.tableView = tableView
        return sessionCell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessions.count
    }

    


}
