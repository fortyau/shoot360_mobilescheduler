//
//  S360SSearchController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/17/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SLocationController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var locationField:UITextField!
    @IBOutlet var searchBtn:UIButton!
    @IBOutlet var errorLbl:FAUErrorLabel!
    
    var locationPicker:UIPickerView = UIPickerView()
    var locationOptions:[String] = ["Hyrule", "Racoon City", "Fillory"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set up navigation
        self.title = "Location"
        self.edgesForExtendedLayout = UIRectEdge.None
        
        //Setup pickers
        locationPicker.delegate = self
        locationPicker.dataSource = self
        locationPicker.showsSelectionIndicator = true
        locationField.inputView = locationPicker
        
        let toolbar:UIToolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.BlackTranslucent
        let doneButton = UIButton()
        doneButton.setTitle("Done", forState: UIControlState.Normal)
                doneButton.addTarget(self, action: #selector(S360SLocationController.locationDoneTouch), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.titleLabel!.font = Fonts.SMALLBUTTON
        doneButton.titleLabel!.textColor = UIColor.whiteColor()
        doneButton.frame = CGRectMake(0.0, 0.0, 100, 30)
        doneButton.backgroundColor = UIColor.blackColor()
        doneButton.layer.cornerRadius = Numbers.CORNERRADIUS
        let doneBarButton:UIBarButtonItem = UIBarButtonItem(customView: doneButton)
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target:nil, action:nil),doneBarButton], animated: false)
        toolbar.sizeToFit()
        toolbar.userInteractionEnabled = true
        locationField.inputAccessoryView = toolbar
        
        //Setup navigation items
        let nav = self.navigationController as! S360SNavigationController
        nav.addNavSwitchButton(self, state: S360SNavigationController.SEARCH)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        //Setup buttons
        searchBtn.enabled = false
        
        //Set up error label
        view.bringSubviewToFront(errorLbl)
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Styling
        locationField.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        locationField.layer.borderWidth = Numbers.BORDERREG
        locationField.layer.cornerRadius = Numbers.CORNERRADIUS
        locationField.tintColor = Colors.SHOOT360RED
        
        searchBtn.layer.cornerRadius = Numbers.CORNERRADIUS
        
        locationPicker.backgroundColor = Colors.REALDARKGREY
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Text Field Delegate
    func textFieldDidBeginEditing(textField:UITextField){
        if textField.text == nil || textField.text == "" {
            textField.text = locationOptions[locationPicker.selectedRowInComponent(0)]
        }
        searchBtn.enabled = true
    }
    
    //Picker Delegate
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label = UILabel()
        label.text = locationOptions[row]
        label.font = Fonts.SPINNER
        label.backgroundColor = Colors.REALDARKGREY
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Left
        return label
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        locationField.text = locationOptions[row]
    }
    
    //Picker Datasource
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return locationOptions.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Button Actions
    func locationDoneTouch(sender:AnyObject){
        locationField.resignFirstResponder()
    }
    
    @IBAction func searchBtnTouch(){
        let schedule = S360SFindSessionController(nibName: XIBFiles.FINDSESSIONVIEW, bundle: nil)
        self.navigationController?.pushViewController(schedule, animated: true)
    }

}
