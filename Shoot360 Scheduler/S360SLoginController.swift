//
//  S360SLoginController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/17/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SLoginController: UIViewController {

    @IBOutlet var emailField:UITextField!
    @IBOutlet var passwordField:UITextField!
    @IBOutlet var loginBtn:UIButton!
    @IBOutlet var errorLbl:FAUErrorLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Fit view
        self.edgesForExtendedLayout = UIRectEdge.None
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        //Styling
        emailField.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        emailField.layer.borderWidth = Numbers.BORDERREG
        emailField.layer.cornerRadius = Numbers.CORNERRADIUS
        emailField.tintColor = Colors.SHOOT360RED
        
        passwordField.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        passwordField.layer.borderWidth = Numbers.BORDERREG
        passwordField.layer.cornerRadius = Numbers.CORNERRADIUS
        passwordField.tintColor = Colors.SHOOT360RED
        
        loginBtn.layer.cornerRadius = Numbers.CORNERRADIUS
        
        //Set up error label
        view.bringSubviewToFront(errorLbl)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    //Button Actions
    @IBAction func loginBtnTouch(){
        self.presentViewController(S360SNavigationController(), animated: true, completion: nil)
    }

}
