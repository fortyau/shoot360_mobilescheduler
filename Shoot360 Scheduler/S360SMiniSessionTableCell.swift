//
//  S360SMiniSessionTableCell.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/19/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SMiniSessionTableCell: UITableViewCell {

    @IBOutlet var startTimeLbl:UILabel!
    @IBOutlet var endTimeLbl:UILabel!
    @IBOutlet var courtsLbl:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
