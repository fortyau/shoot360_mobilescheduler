//
//  S360SMyScheduleController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/18/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit
import SCLAlertView

class S360SMyScheduleController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var logOutBtn:UIButton!
    @IBOutlet var scheduleTbl:UITableView!
    @IBOutlet var errorLbl:FAUErrorLabel!
    
    //TODO: THIS IS JUST DEMO CODE
    var dateFormatter = NSDateFormatter()
    var sessions:[[String:AnyObject]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set up navigation
        self.title = "My Schedule"
        self.edgesForExtendedLayout = UIRectEdge.None

        //Setup navigation items
        let nav = self.navigationController as! S360SNavigationController
        nav.addNavSwitchButton(self, state: S360SNavigationController.PROFILE)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        //Set up error label
        view.bringSubviewToFront(errorLbl)
        
        //TODO: THIS IS JUST DEMO CODE
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        sessions = [
            [
                "location":"Narnia",
                "court":1,
                "startDate":dateFormatter.dateFromString("2016-05-23 17:00:00")!,
                "endDate":dateFormatter.dateFromString("2016-05-23 20:00:00")!,
                "eventType":"Ball Handling"
            ],
            [
                "location":"Fillory",
                "court":1,
                "startDate":dateFormatter.dateFromString("2016-05-23 20:00:00")!,
                "endDate":dateFormatter.dateFromString("2016-05-23 21:00:00")!,
                "eventType":"Shooting"
            ],
            [
                "location":"Racoon City",
                "court":1,
                "startDate":dateFormatter.dateFromString("2016-05-29 17:00:00")!,
                "endDate":dateFormatter.dateFromString("2016-05-29 20:00:00")!,
                "eventType":"Personal Training"
            ],
            [
                "location":"Narnia",
                "court":1,
                "startDate":dateFormatter.dateFromString("2016-06-23 17:00:00")!,
                "endDate":dateFormatter.dateFromString("2016-06-23 20:00:00")!,
                "eventType":"Ball Handling"
            ],
            [
                "location":"Narnia",
                "court":1,
                "startDate":dateFormatter.dateFromString("2016-06-23 17:00:00")!,
                "endDate":dateFormatter.dateFromString("2016-06-23 20:00:00")!,
                "eventType":"Shooting"
            ]
            
        ]
        
        
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Styling
        logOutBtn.layer.cornerRadius = Numbers.CORNERRADIUS
        
        scheduleTbl.rowHeight = UITableViewAutomaticDimension
        scheduleTbl.estimatedRowHeight = 100.0
        scheduleTbl.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        scheduleTbl.layer.borderWidth = Numbers.BORDERREG
        scheduleTbl.layer.cornerRadius = Numbers.CORNERRADIUS
        scheduleTbl.separatorColor = Colors.REALLIGHTGREY
        scheduleTbl.separatorInset = UIEdgeInsetsZero
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Button Actions
    @IBAction func logOutBtnTouch(){
        self.navigationController!.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
    }

    //Table Delegate/Datasource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:S360SSessionTableCell? = tableView.dequeueReusableCellWithIdentifier(XIBFiles.SESSIONTABLECELL) as? S360SSessionTableCell
        
        if ((cell == nil)){
            tableView.registerNib(UINib(nibName: XIBFiles.SESSIONTABLECELL, bundle: nil), forCellReuseIdentifier: XIBFiles.SESSIONTABLECELL)
            cell = tableView.dequeueReusableCellWithIdentifier(XIBFiles.SESSIONTABLECELL) as? S360SSessionTableCell
        }
        
        var session = sessions[indexPath.row]
        cell!.setIcon(session["eventType"] as! String)
        cell!.locationLbl.text = (session["location"] as! String)
        cell!.courtLbl.text = "Court " + String(session["court"]!)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        cell!.dateLbl.text = dateFormatter.stringFromDate(session["startDate"] as! NSDate)
        dateFormatter.dateFormat = "hh:mm a"
        cell!.startTimeLbl.text = dateFormatter.stringFromDate(session["startDate"] as! NSDate)
        cell!.endTimeLbl.text = dateFormatter.stringFromDate(session["endDate"] as! NSDate)
        cell!.separatorInset = UIEdgeInsetsZero
        cell!.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessions.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var session = sessions[indexPath.row]
        
        //This is split up because XCode can't handle compiling this because it is 'too complex', which is rather silly, but just the way things are.
        var descript = "\n" + (session["eventType"] as! String)
        descript = descript + "\nCourt " + String(session["court"]!)
        dateFormatter.dateFormat = "hh:mm a"
        descript = descript + "\n" + dateFormatter.stringFromDate(session["startDate"]! as! NSDate) + " to " + dateFormatter.stringFromDate(session["endDate"]! as! NSDate) + "\n"
        
        //        let alertAppearance = SCLAlertView.SCLAppearance(
        //            kDefaultShadowOpacity: <#T##CGFloat#>,
        //            kCircleTopPosition: <#T##CGFloat#>,
        //            kCircleBackgroundTopPosition: <#T##CGFloat#>,
        //            kCircleHeight: <#T##CGFloat#>,
        //            kCircleIconHeight: <#T##CGFloat#>,
        //            kTitleTop: <#T##CGFloat#>,
        //            kTitleHeight: <#T##CGFloat#>,
        //            kWindowWidth: <#T##CGFloat#>,
        //            kWindowHeight: <#T##CGFloat#>,
        //            kTextHeight: <#T##CGFloat#>,
        //            kTextFieldHeight: <#T##CGFloat#>,
        //            kTextViewdHeight: <#T##CGFloat#>,
        //            kButtonHeight: <#T##CGFloat#>,
        //            kTitleFont: <#T##UIFont#>,
        //            kTextFont: <#T##UIFont#>,
        //            kButtonFont: <#T##UIFont#>,
        //            showCloseButton: <#T##Bool#>,
        //            showCircularIcon: <#T##Bool#>,
        //            shouldAutoDismiss: <#T##Bool#>,
        //            contentViewCornerRadius: <#T##CGFloat#>,
        //            fieldCornerRadius: <#T##CGFloat#>,
        //            buttonCornerRadius: <#T##CGFloat#>,
        //            hideWhenBackgroundViewIsTapped: <#T##Bool#>,
        //            contentViewColor: <#T##UIColor#>,
        //            contentViewBorderColor: <#T##UIColor#>,
        //            titleColor: <#T##UIColor#>
        //        )
        
        let alertAppearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            kTextFont: Fonts.BODY,
            kTitleFont: Fonts.TITLESMALL,
            kButtonFont: Fonts.BUTTON,
            buttonCornerRadius: Numbers.CORNERRADIUS,
            kCircleIconHeight: Numbers.ALERTICONHEIGHT,
            kCircleHeight: Numbers.ALERTCIRCLEHEIGHT
        )
        
        let scheduleAlert = SCLAlertView(appearance: alertAppearance)
        scheduleAlert.addButton("Unschedule", backgroundColor: Colors.SHOOT360RED, textColor: UIColor.whiteColor(), showDurationStatus: false, action: {
            self.sessions.removeAtIndex(indexPath.row)
            self.errorLbl.displayMessage(FAUErrorLabel.MessageLevel.INFO, message: "Session Unscheduled")
            self.scheduleTbl.reloadData()
        })
        scheduleAlert.addButton("Done", action: {})
        dateFormatter.dateFormat = "MM/dd/yyyy"
        scheduleAlert.showTitle((session["location"]! as! String) + " - " + (dateFormatter.stringFromDate(session["startDate"] as! NSDate)), subTitle: descript, style: .Info, circleIconImage: Images.LOGOSOLO)
    }


}
