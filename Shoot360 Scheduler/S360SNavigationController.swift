//
//  S360SNavigationController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/18/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SNavigationController: UINavigationController {
    
    var controllersSearch:[UIViewController] = []
    var controllersProfile:[UIViewController] = []
    
    internal static var SEARCH = 0
    internal static var PROFILE = 1
    
    private var state = -1
    
    var navSwitchBtn:UIBarButtonItem = UIBarButtonItem(title: "Profile", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.controllersSearch.append(S360SLocationController(nibName: XIBFiles.LOCATIONVIEW, bundle: nil))
        
        self.controllersProfile.append(S360SMyScheduleController(nibName: XIBFiles.MYSCHEDULEVIEW, bundle: nil))
        
        self.state = S360SNavigationController.SEARCH
        
        self.viewControllers = controllersSearch
        
        navSwitchBtn.target = self
        navSwitchBtn.action = #selector(S360SNavigationController.navSwitchTouch)
        
        //Styling
        self.navigationBar.tintColor = UIColor.blackColor()
        
        navSwitchBtn.setTitleTextAttributes([NSFontAttributeName: Fonts.NAVBARICONS, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Adds nav switch to any view controller
    func addNavSwitchButton(viewController:UIViewController, state:Int) {
        self.setupNavSwitchState(state)
        viewController.navigationItem.rightBarButtonItem = navSwitchBtn
    }
    
    //Sets up the state of the nav switch
    private func setupNavSwitchState(state:Int){
        //Set up button state
        switch(state){
            case S360SNavigationController.SEARCH:
                self.state = S360SNavigationController.SEARCH
                self.navSwitchBtn.title = Icons.PROFILE
                break;
            case S360SNavigationController.PROFILE:
                self.state = S360SNavigationController.PROFILE
                self.navSwitchBtn.title = Icons.SEARCH
                break;
            default:
                self.state = S360SNavigationController.SEARCH
                self.navSwitchBtn.title = Icons.PROFILE
                break;
        }

    }
    
    //Resets the view controllers of the search
    func resetSearch(){
        controllersSearch = [controllersSearch[0]]
    }
    
    //Navigated between the two views
    func navSwitchTouch(){
        switch(self.state){
            case S360SNavigationController.SEARCH:
                setupNavSwitchState(S360SNavigationController.PROFILE)
                controllersSearch = self.viewControllers
                self.setViewControllers(controllersProfile, animated: true)
                break;
            case S360SNavigationController.PROFILE:
                setupNavSwitchState(S360SNavigationController.SEARCH)
                controllersProfile = self.viewControllers
                self.setViewControllers(controllersSearch, animated: true)
                break;
            default:
                setupNavSwitchState(S360SNavigationController.PROFILE)
                controllersSearch = self.viewControllers
                self.setViewControllers(controllersProfile, animated: true)
                break;
        }
    }

}
