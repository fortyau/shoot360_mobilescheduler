//
//  S360SSessionMatchEventView.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 7/1/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SScheduledEventView: UIView {

    @IBOutlet var iconImg:UIImageView!
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var timeLbl:UILabel!
    @IBOutlet var courtLbl:UILabel!

}
