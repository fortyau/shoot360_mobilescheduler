//
//  S360SEventTypeCell.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 6/6/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SSessionEventTypeTableCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource, S360SEventTypeView {
    
    @IBOutlet var iconImg:UIImageView!
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var timeField:UITextField!
    @IBOutlet var durationLbl:UILabel!
    @IBOutlet var minsLbl:UILabel!
    
    var tableView:UITableView?
    var eventType:S360SEvent.S360SEventType!
    var lowMins = 10
    var highMins = 120
    var mins:NSMutableArray = []
    var minsPicker:UIPickerView = UIPickerView()
    var session:NSMutableDictionary = [
        "title": "Default",
        "minutes": 30
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.showsReorderControl = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Setup pickers
        var num = lowMins
        while num <= highMins {
            mins.addObject(num)
            num += 10
        }
        minsPicker.delegate = self
        minsPicker.dataSource = self
        minsPicker.showsSelectionIndicator = true
        minsPicker.backgroundColor = Colors.REALDARKGREY
        timeField.inputView = minsPicker
        
        let toolbar:UIToolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.BlackTranslucent
        let doneButton = UIButton()
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.addTarget(self, action: #selector(S360SSessionEventTypeTableCell.minsDoneTouch), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.titleLabel!.font = Fonts.SMALLBUTTON
        doneButton.titleLabel!.textColor = UIColor.whiteColor()
        doneButton.frame = CGRectMake(0.0, 0.0, 100, 30)
        doneButton.backgroundColor = UIColor.blackColor()
        doneButton.layer.cornerRadius = Numbers.CORNERRADIUS
        let doneBarButton:UIBarButtonItem = UIBarButtonItem(customView: doneButton)
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target:nil, action:nil),doneBarButton], animated: false)
        toolbar.sizeToFit()
        toolbar.userInteractionEnabled = true
        timeField.inputAccessoryView = toolbar
        
        timeField.text = String(self.session["minutes"] as! Int)
        
        //Setup event type
        titleLbl.text = eventType.rawValue
        iconImg.image = Images.getEventImage(eventType)
        
        //Styling
        timeField.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        timeField.layer.borderWidth = Numbers.BORDERREG
        timeField.layer.cornerRadius = Numbers.CORNERRADIUS
        timeField.tintColor = Colors.SHOOT360RED
        
        
    }
    
    //Event type methods
    func getValue() -> Int{
        return Int(timeField.text!)!
    }
    
    func getType() -> S360SEvent.S360SEventType {
        return eventType
    }
    
    //Text Field Delegate
    func textFieldDidBeginEditing(textField:UITextField){
        if textField.text == nil || textField.text == "" {
            textField.text = String(mins[minsPicker.selectedRowInComponent(0)])
        }
    }
    
    //Picker Delegate
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label = UILabel()
        label.text = String(mins[row])
        label.font = Fonts.SPINNER
        label.backgroundColor = Colors.REALDARKGREY
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Left
        return label
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        timeField.text = String(mins[row])
    }
    
    //Picker Datasource
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return mins.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Button/Switch Actions
    func minsDoneTouch(sender:AnyObject){
        self.session.setObject(mins[minsPicker.selectedRowInComponent(0)], forKey: "minutes")
        self.timeField.text = String(session["minutes"] as! Int)
        self.tableView?.reloadData()
        self.timeField.resignFirstResponder()
    }

}
