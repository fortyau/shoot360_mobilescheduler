//
//  S360SSessionMatchTableCell.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 6/8/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SSessionMatchTableCell: UITableViewCell {
    
    @IBOutlet var startTimeLbl:UILabel!
    @IBOutlet var toLbl:UILabel!
    @IBOutlet var endTimeLbl:UILabel!
    @IBOutlet var starsVw:UIView!
    
    var height:CGFloat = 200.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func addStars(numStars:Int){
                var starImg:UIImageView!
                var count = numStars
                var lastStarImg:UIImageView? = nil
                while count != 0 {
                    starImg = UIImageView(image: Images.STAR)
                    starImg.translatesAutoresizingMaskIntoConstraints = false
        
                    starsVw.addSubview(starImg)
                    starsVw.addConstraint(NSLayoutConstraint(item: starImg, attribute: .Top, relatedBy: .Equal, toItem: starsVw, attribute: .Top, multiplier: 1, constant: 0))
                    starsVw.addConstraint(NSLayoutConstraint(item: starImg, attribute: .Bottom, relatedBy: .Equal, toItem: starsVw, attribute: .Bottom, multiplier: 1, constant: 0))
                    starImg.addConstraint(NSLayoutConstraint(item: starImg, attribute: .Width, relatedBy: .Equal, toItem: starImg, attribute: .Height, multiplier: 1, constant: 0))
        
                    if lastStarImg == nil {
                        starsVw.addConstraint(NSLayoutConstraint(item: starImg, attribute: .Trailing, relatedBy: .Equal, toItem: starsVw, attribute: .Trailing, multiplier: 1, constant: 0))
                    }
                    else {
                        starsVw.addConstraint(NSLayoutConstraint(item: lastStarImg!, attribute: .Leading, relatedBy: .Equal, toItem: starImg, attribute: .Trailing, multiplier: 1, constant: 0))
                    }
        
                    lastStarImg = starImg
                    count -= 1
                }
                self.layoutSubviews()
    }
    
    func setupEvents(events:[[String:String]]){
        
        //Start height setup
        height = toLbl.frame.size.height + 30
        
        //Set up start and end times
        self.startTimeLbl.text = events[0]["startTime"]!
        self.endTimeLbl.text = events[events.count - 1]["endTime"]!
        
        //Set up events
        var pastEventView:S360SScheduledEventView? = nil
        var pastEvent:[String:String]? = nil
        for (index, event) in events.enumerate(){
            var topAnchor:NSLayoutConstraint!
            
            //Create event view
            let eventView:S360SScheduledEventView = NSBundle.mainBundle().loadNibNamed(XIBFiles.SCHEDULEDEVENTVIEW, owner: self, options: nil)[0] as! S360SScheduledEventView
            
            //Deal with first view added
            if pastEvent == nil{
                
                //Top anchor setup for first view
                topAnchor = NSLayoutConstraint(item: eventView, attribute: .Top, relatedBy: .Equal, toItem: toLbl, attribute: .Bottom, multiplier: 1, constant: 10)
            }
            else{
                
                //Check for a break
                let timeFormatter:NSDateFormatter = NSDateFormatter()
                timeFormatter.dateFormat = "hh:mm a"
                let startTime = timeFormatter.dateFromString(pastEvent!["endTime"]!)
                let endTime = timeFormatter.dateFromString(event["startTime"]!)
                if startTime != endTime {
                    
                    //Create break view
                    let breakView = NSBundle.mainBundle().loadNibNamed(XIBFiles.SCHEDULEDBREAKVIEW, owner: self, options: nil)[0] as! S360SScheduledBreakView
                    breakView.minsLbl.text = String(Int(endTime!.timeIntervalSinceDate(startTime!)/60)) + " Minutes"
                    
                    //Setup breakview constraints
                    let bTopAnchor = NSLayoutConstraint(item: breakView, attribute: .Top, relatedBy: .Equal, toItem: pastEventView, attribute: .Bottom, multiplier: 1, constant: 0)
                    let bLeftAnchor = NSLayoutConstraint(item: breakView, attribute: .Leading, relatedBy: .Equal, toItem: self.contentView, attribute: .LeadingMargin, multiplier: 1, constant: 0)
                    let bRightAnchor = NSLayoutConstraint(item: breakView, attribute: .Trailing, relatedBy: .Equal, toItem: self.contentView, attribute: .TrailingMargin, multiplier: 1, constant: 0)
                    let bHeightAnchor = NSLayoutConstraint(item: breakView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 30)
                    
                    //Add break view to height
                    height += 30
                    
                    //Add break view and constraints
                    self.addSubview(breakView)
                    self.addConstraints([bTopAnchor, bLeftAnchor, bRightAnchor, bHeightAnchor])
                    
                    //Top anchor setup for subsequent view
                    topAnchor = NSLayoutConstraint(item: eventView, attribute: .Top, relatedBy: .Equal, toItem: breakView, attribute: .Bottom, multiplier: 1, constant: 0)
                }
                else{
                    
                    //Top anchor setup for subsequent views
                    topAnchor = NSLayoutConstraint(item: eventView, attribute: .Top, relatedBy: .Equal, toItem: pastEventView, attribute: .Bottom, multiplier: 1, constant: 0)
                }
                
            }
            
            //Setup other anchors
            let leftAnchor = NSLayoutConstraint(item: eventView, attribute: .Leading, relatedBy: .Equal, toItem: self.contentView, attribute: .LeadingMargin, multiplier: 1, constant: 0)
            let rightAnchor = NSLayoutConstraint(item: eventView, attribute: .Trailing, relatedBy: .Equal, toItem: self.contentView, attribute: .TrailingMargin, multiplier: 1, constant: 0)
            let heightAnchor = NSLayoutConstraint(item: eventView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 60)
            
            //Add event view to height
            height += 60
            
            //Setup event view
            eventView.iconImg.image = Images.getEventImage(event["title"]!)
            eventView.titleLbl.text = event["title"]!
            eventView.courtLbl.text = "court" + event["court"]!
            eventView.timeLbl.text = event["startTime"]! + " to " + event["endTime"]!
            
            //Add event view and constraints
            self.addSubview(eventView)
            self.addConstraints([topAnchor, leftAnchor, rightAnchor, heightAnchor])
            
            //Prepare for next iteration
            pastEventView = eventView
            pastEvent = event
            
            //Set up last cell with bottom bound
            if index == events.count - 1 {
                let bottomAnchor = NSLayoutConstraint(item: eventView, attribute: .Bottom, relatedBy: .Equal, toItem: self.contentView, attribute: .BottomMargin, multiplier: 1, constant: 0)
                self.addConstraint(bottomAnchor)
            }
            
        }
    }
    
}
