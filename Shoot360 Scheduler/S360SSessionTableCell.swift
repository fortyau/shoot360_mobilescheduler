//
//  S360SSessionTableCell.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 5/18/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit

class S360SSessionTableCell: UITableViewCell {

    @IBOutlet var iconImg:UIImageView!
    @IBOutlet var locationLbl:UILabel!
    @IBOutlet var dateLbl:UILabel!
    @IBOutlet var startTimeLbl:UILabel!
    @IBOutlet var endTimeLbl:UILabel!
    @IBOutlet var courtLbl:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setIcon(eventType:String){
        iconImg.image = Images.getEventImage(eventType)
    }

}
