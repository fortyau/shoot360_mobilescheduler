//
//  S360SSessionMatchController.swift
//  Shoot360 Scheduler
//
//  Created by Steven T. Norris on 6/8/16.
//  Copyright © 2016 FortyAU. All rights reserved.
//

import UIKit
import SCLAlertView

class S360SSessionsMatchController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var locationLbl:UILabel!
    @IBOutlet var showAllBtn:UIButton!
    @IBOutlet var sessionsTbl:UITableView!
    @IBOutlet var errorLbl:FAUErrorLabel!
    
    var sessionMatches = []
    var timeFormatter:NSDateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set up navigation
        self.title = "Found Sessions"
        self.edgesForExtendedLayout = UIRectEdge.None
        
        //Setup navigation items
        let nav = self.navigationController as! S360SNavigationController
        nav.addNavSwitchButton(self, state: S360SNavigationController.SEARCH)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        //Setup location label
        self.locationLbl.text = "Fillory - 08/23/2016"

        //Set up helpers
        timeFormatter.dateFormat = "hh:mm a"
        
        //Set up error label
        view.bringSubviewToFront(errorLbl)
        
        //TODO: DEMO CODE ONLY
        sessionMatches = [
            [
                "sessions":[
                    [
                        "title":"Ball Handling",
                        "startTime":"10:00 AM",
                        "endTime":"10:30 AM",
                        "court":"1"
                    ],
                    [
                        "title":"Shooting",
                        "startTime":"10:30 AM",
                        "endTime":"11:00 AM",
                        "court":"3"
                    ],
                    [
                        "title":"Train360",
                        "startTime":"11:00 AM",
                        "endTime":"11:30 PM",
                        "court":"4"
                    ]
                ],
                "rating":4
            ],
            [
                "sessions":[
                    [
                        "title":"Ball Handling",
                        "startTime":"10:00 AM",
                        "endTime":"10:30 AM",
                        "court":"1"
                    ],
                    [
                        "title":"Shooting",
                        "startTime":"10:50 AM",
                        "endTime":"11:20 AM",
                        "court":"1"
                    ],
                    [
                        "title":"Train360",
                        "startTime":"11:40 AM",
                        "endTime":"12:10 PM",
                        "court":"1"
                    ]
                ],
                "rating":3
            ],
            [
                "sessions":[
                    [
                        "title":"Ball Handling",
                        "startTime":"10:00 AM",
                        "endTime":"10:30 AM",
                        "court":"5"
                    ],
                    [
                        "title":"Shooting",
                        "startTime":"10:30 AM",
                        "endTime":"11:00 AM",
                        "court":"5"
                    ]
                ],
                "rating":2
            ],
            [
                "sessions":[
                    [
                        "title":"Ball Handling",
                        "startTime":"10:00 AM",
                        "endTime":"10:30 AM",
                        "court":"6"
                    ]
                ],
                "rating":1
            ]
            
        ]
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Styling
        showAllBtn.layer.cornerRadius = Numbers.CORNERRADIUS
        
        //sessionsTbl.rowHeight = UITableViewAutomaticDimension
        sessionsTbl.rowHeight = 300
        sessionsTbl.estimatedRowHeight = 200
        sessionsTbl.layer.borderColor = Colors.REALLIGHTGREY.CGColor
        sessionsTbl.layer.borderWidth = Numbers.BORDERREG
        sessionsTbl.layer.cornerRadius = Numbers.CORNERRADIUS
        sessionsTbl.separatorStyle = UITableViewCellSeparatorStyle.None
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Button Actions
    @IBAction func showAllBtnTouch(){
        let allSessions = S360SAllSessionsController(nibName: XIBFiles.ALLSESSIONSVIEW, bundle: nil)
        self.navigationController?.pushViewController(allSessions, animated: true)
    }

    //Table Delegate/Datasource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:S360SSessionMatchTableCell? = tableView.dequeueReusableCellWithIdentifier(XIBFiles.SESSIONMATCHTABLECELL + String(indexPath.row)) as? S360SSessionMatchTableCell
        
        if ((cell == nil)){
            tableView.registerNib(UINib(nibName: XIBFiles.SESSIONMATCHTABLECELL, bundle: nil), forCellReuseIdentifier: XIBFiles.SESSIONMATCHTABLECELL + String(indexPath.row))
            cell = tableView.dequeueReusableCellWithIdentifier(XIBFiles.SESSIONMATCHTABLECELL + String(indexPath.row)) as? S360SSessionMatchTableCell
        }
        
        cell!.setupEvents(sessionMatches[indexPath.row]["sessions"]! as! [[String:String]])
        cell!.addStars(sessionMatches[indexPath.row]["rating"]! as! Int)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //return UITableViewAutomaticDimension
        return 300
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessionMatches.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let sessions = sessionMatches[indexPath.row]["sessions"] as! [[String:String]]
        
        var descript = "\n"
        
        for session in sessions {
            descript = descript + session["title"]! + " - " + session["startTime"]! + " to " + session["endTime"]! + "\n"

        }
        
//        let alertAppearance = SCLAlertView.SCLAppearance(
//            kDefaultShadowOpacity: <#T##CGFloat#>,
//            kCircleTopPosition: <#T##CGFloat#>,
//            kCircleBackgroundTopPosition: <#T##CGFloat#>,
//            kCircleHeight: <#T##CGFloat#>,
//            kCircleIconHeight: <#T##CGFloat#>,
//            kTitleTop: <#T##CGFloat#>,
//            kTitleHeight: <#T##CGFloat#>,
//            kWindowWidth: <#T##CGFloat#>,
//            kWindowHeight: <#T##CGFloat#>,
//            kTextHeight: <#T##CGFloat#>,
//            kTextFieldHeight: <#T##CGFloat#>,
//            kTextViewdHeight: <#T##CGFloat#>,
//            kButtonHeight: <#T##CGFloat#>,
//            kTitleFont: <#T##UIFont#>,
//            kTextFont: <#T##UIFont#>,
//            kButtonFont: <#T##UIFont#>,
//            showCloseButton: <#T##Bool#>,
//            showCircularIcon: <#T##Bool#>,
//            shouldAutoDismiss: <#T##Bool#>,
//            contentViewCornerRadius: <#T##CGFloat#>,
//            fieldCornerRadius: <#T##CGFloat#>,
//            buttonCornerRadius: <#T##CGFloat#>,
//            hideWhenBackgroundViewIsTapped: <#T##Bool#>,
//            contentViewColor: <#T##UIColor#>,
//            contentViewBorderColor: <#T##UIColor#>,
//            titleColor: <#T##UIColor#>
//        )
        
        let alertAppearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            kTextFont: Fonts.BODY,
            kTitleFont: Fonts.TITLEMID,
            kButtonFont: Fonts.BUTTON,
            buttonCornerRadius: Numbers.CORNERRADIUS,
            kCircleIconHeight: Numbers.ALERTICONHEIGHT,
            kCircleHeight: Numbers.ALERTCIRCLEHEIGHT
        )
        
        let scheduleAlert = SCLAlertView(appearance: alertAppearance)
        scheduleAlert.addButton("Schedule", action: {
            let nav = self.navigationController as! S360SNavigationController
            nav.navSwitchTouch()
            nav.resetSearch()
        })
        scheduleAlert.addButton("Cancel", action: {})
        scheduleAlert.showTitle("Fillory - 09/23/2016", subTitle: descript, style: .Info, circleIconImage: Images.LOGOSOLO)
    }

    
}
